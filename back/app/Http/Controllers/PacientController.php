<?php

namespace App\Http\Controllers;

use App\Models\Pacient;
use App\Models\PacientPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use Illuminate\Validation\Validator;

class PacientController extends Controller
{

    private Pacient $model;
    private PacientPhoto $photo;
    public function __construct(Pacient $model, PacientPhoto $photo)
    {
        $this->model = $model;
        $this->photo = $photo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->model->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->model->create($request->all());
        return response()->json([
            'message' => 'Paciente creado correctamente'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pacient  $pacient
     * @return \Illuminate\Http\Response
     */
    public function show(Pacient $pacient)
    {
        return response()->json($pacient, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pacient  $pacient
     * @return \Illuminate\Http\Response
     */
    public function edit(Pacient $pacient)
    {
        return response()->json($pacient, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pacient  $pacient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pacient $pacient)
    {
        $pacient->update($request->all());
        return response()->json([
            'message' => 'Paciente actualizado correctamente',
        ], 200);        
    }

    public function search(Request $request)
    {
        $search = $this->model->where(function($query) use ($request) {
            $query->orWhere(DB::raw('CONCAT(first_name," ",last_name)'), 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('first_name', 'LIKE', "%{$request['search']}%");
        });
        return response()->json($search->get(), 200);
    }

    public function updatePhoto(Request $request)
    {       
        //$mode -> escoge que foto quieres actualizar
        $pacient = $this->model->find($request->pacient_id);
        if(!$pacient) 
        {
            return response()->json([
                'message' => 'Lo sentimos, no pudimos encontrar al paciente'
            ]);
        }
        $photoName = '';
        if ($file = $request->file('photo')) {
            $photoName = $file->store('public/files');
        }        
        
        if($request->mode == 1){
            $pacient->photo1 = Storage::url($photoName);
        }else if($request->mode == 2){
            $pacient->photo2 = Storage::url($photoName);
        }else if($request->mode == 3){
            $pacient->photo3 = Storage::url($photoName);
        }

        $pacient->save();

        return response()->json([
            'message' => 'Se actualizo la foto correctamente',
            'pacient' => $pacient
        ], 200);
    }

    public function updateMorePhotos(Request $request,int $patient_id)
    {
        $pacient = $this->model->find($patient_id);
        if(!$pacient) 
        {
            return response()->json([
                'message' => 'Lo sentimos, no pudimos encontrar al paciente'
            ]);
        }       

        $photoName = '';
        if ($file = $request->file('photo')) {
            $photoName = $file->store('public/files');
        }     
        
        $photo = $this->photo->create([
            'pacient_id' => $patient_id,
            'url' => Storage::url($photoName),
            'name' => $photoName
        ]);

        return response()->json([
            'message' => 'Se agrego la foto correctamente',
            'pacient' => $photo
        ], 200);         

    }

    public function getAllPhotos(int $patient_id)
    {
        return response()->json($this->model->find($patient_id)->photos()->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pacient  $pacient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pacient $pacient)
    {
        $pacient->delete();
        return response()->json([
            'message' => 'Paciente eliminado correctamente',
        ], 200);
    }
}
