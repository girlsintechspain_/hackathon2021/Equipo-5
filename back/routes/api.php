<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\PacientController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [LoginController::class, 'login']);

Route::middleware('auth:sanctum')->group(function() {
    Route::post('patients/photo', [PacientController::class, 'updatePhoto']);
    Route::post('patients/photos/{patient_id}', [PacientController::class, 'updateMorePhotos']);
    Route::get('patients/photos/{patient_id}', [PacientController::class, 'getAllPhotos']);
    Route::post('patients/search', [PacientController::class, 'search']);
    Route::resource('patients', PacientController::class);
});
