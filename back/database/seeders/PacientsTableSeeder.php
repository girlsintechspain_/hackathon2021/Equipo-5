<?php

namespace Database\Seeders;

use App\Models\Pacient;
use Illuminate\Database\Seeder;

class PacientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pacient::factory(20)->create();
    }
}
