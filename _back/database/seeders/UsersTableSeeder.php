<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    private User $user;
    public function __construct(User $user)
    {
        $this->model = $user;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->model->create([
            'name' => 'User prueba',
            'email' => 'usertest@mail.com',
            'password' => bcrypt('password')
        ]);
    }
}
