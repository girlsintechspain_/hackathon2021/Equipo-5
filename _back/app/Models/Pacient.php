<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pacient extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'user_id',
        'photo1',
        'photo2',
        'photo3'
    ];

    public function photos()
    {
        return $this->hasMany(PacientPhoto::class);
    }
}
