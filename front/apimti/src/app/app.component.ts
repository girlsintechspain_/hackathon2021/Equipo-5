import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/home', icon: 'home' },
    { title: 'Main', url: '/main', icon: 'flower' },
    { title: 'Login', url: '/login', icon: 'log-in' },
    { title: 'Profile', url: '/profile', icon: 'person' },
    { title: 'Register', url: '/register', icon: 'person-add' },
    { title: 'Register-detail', url: '/register-detail', icon: 'person-add' },
    { title: 'Services', url: '/services', icon: 'hand-right' },
    { title: 'Services-detail', url: '/service-detail', icon: 'hand-right' },
    { title: 'Advices', url: '/advices', icon: 'book' },
    { title: 'Advices-detail', url: '/advice-detail', icon: 'book' },
    { title: 'Chat', url: '/chat', icon: 'chatbubbles' },
    { title: 'Chat-detail', url: '/chat-detail', icon: 'chatbubbles' },
    { title: 'Schedule', url: '/schedule', icon: 'calendar-number' },
    
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
